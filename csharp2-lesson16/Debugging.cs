﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Debugging
{
    public partial class Debugging : Form
    {
        private Label enterTextLabel;
        private Label label2;
        private Label letterCountValueLabel;
        private Label wordCountValueLabel;
        private Button countButton;
        private TextBox dataTextBox;
        private Button closeButton;
        private Label label3;
    
        public Debugging()
        {
            InitializeComponent();
        }

        private int countWords(string textData)
        {
            int wordCount = 0;
            wordCount = Regex.Matches(textData, @"[A-Za-z0-9]+").Count;
            return wordCount;
        }

        private int countLetters(string textData)
        {
            int letterCount = 0;
            letterCount = Regex.Matches(textData, @"[A-Za-z]").Count;
            return letterCount;
        }

        private void InitializeComponent()
        {
            this.enterTextLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.letterCountValueLabel = new System.Windows.Forms.Label();
            this.wordCountValueLabel = new System.Windows.Forms.Label();
            this.countButton = new System.Windows.Forms.Button();
            this.dataTextBox = new System.Windows.Forms.TextBox();
            this.closeButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // enterTextLabel
            // 
            this.enterTextLabel.AutoSize = true;
            this.enterTextLabel.Location = new System.Drawing.Point(27, 42);
            this.enterTextLabel.Name = "enterTextLabel";
            this.enterTextLabel.Size = new System.Drawing.Size(56, 13);
            this.enterTextLabel.TabIndex = 0;
            this.enterTextLabel.Text = "Enter Text";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Letter Count";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Word Count";
            // 
            // letterCountValueLabel
            // 
            this.letterCountValueLabel.AutoSize = true;
            this.letterCountValueLabel.Location = new System.Drawing.Point(115, 82);
            this.letterCountValueLabel.Name = "letterCountValueLabel";
            this.letterCountValueLabel.Size = new System.Drawing.Size(13, 13);
            this.letterCountValueLabel.TabIndex = 3;
            this.letterCountValueLabel.Text = "0";
            // 
            // wordCountValueLabel
            // 
            this.wordCountValueLabel.AutoSize = true;
            this.wordCountValueLabel.Location = new System.Drawing.Point(118, 125);
            this.wordCountValueLabel.Name = "wordCountValueLabel";
            this.wordCountValueLabel.Size = new System.Drawing.Size(13, 13);
            this.wordCountValueLabel.TabIndex = 4;
            this.wordCountValueLabel.Text = "0";
            // 
            // countButton
            // 
            this.countButton.Location = new System.Drawing.Point(30, 175);
            this.countButton.Name = "countButton";
            this.countButton.Size = new System.Drawing.Size(75, 23);
            this.countButton.TabIndex = 5;
            this.countButton.Text = "Count";
            this.countButton.UseVisualStyleBackColor = true;
            this.countButton.Click += new System.EventHandler(this.countButton_Click_1);
            // 
            // dataTextBox
            // 
            this.dataTextBox.Location = new System.Drawing.Point(118, 42);
            this.dataTextBox.Name = "dataTextBox";
            this.dataTextBox.Size = new System.Drawing.Size(100, 20);
            this.dataTextBox.TabIndex = 6;
            // 
            // closeButton
            // 
            this.closeButton.Location = new System.Drawing.Point(163, 175);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 7;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click_1);
            // 
            // Debugging
            // 
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.dataTextBox);
            this.Controls.Add(this.countButton);
            this.Controls.Add(this.wordCountValueLabel);
            this.Controls.Add(this.letterCountValueLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.enterTextLabel);
            this.Name = "Debugging";
            this.Text = "Debugging";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void countButton_Click_1(object sender, EventArgs e)
        {
            letterCountValueLabel.Text = countLetters(dataTextBox.Text).ToString();
            wordCountValueLabel.Text = countWords(dataTextBox.Text).ToString();
        }

        private void closeButton_Click_1(object sender, EventArgs e)
        {
            Close();
        }
    }
}

